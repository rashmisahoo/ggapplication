<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<script>
String translationsList=document.getElementById("Translations:").value();
obj = JSON.parse(translationsList);
</script>
<form action="">
	<h2>List of Configuration</h2>
	<table>
		<tr>
			<td>Translations ID</td>
			<td>Module Name</td>
			<td>Key Name</td>
			<td>Value Details</td>
			<td></td>
		</tr>
		<c:forEach items="${obj}" var="translations">
			<tr>
				<td>${translations.id}</td>
				<td>${translations.module}</td>
				<td>${translations.key}</td>
				<td>${translations.value}</td>
				<td><a href="<c:url value='' />">delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<br />
	<a href="<c:url value='/new' />">Add New Configuration</a>
	</form>
</body>
</html>