package com.happiestminds.translations.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import hm.gg.translations.facade.TranslationsFacade;
import hm.gg.translations.model.Translations;


@Controller
//@RequestMapping(value="/GGTranslationsWeb")
public class TranslationsController {
	
	@Autowired(required = true)
	private TranslationsFacade transFacade;
	
	@RequestMapping(method = RequestMethod.POST,value = "/addConfig")
	@ResponseBody
	
	public String addConfiguration(final HttpServletRequest req , HttpServletResponse res ,Model model) throws Exception {
		System.out.println("its here in controller");
		int id=Integer.parseInt(req.getParameter("id"));
		String module=req.getParameter("module");
		String key=req.getParameter("key");
		String value=req.getParameter("value");
		Translations translations = new Translations();
		translations.setGg_key(key);
		translations.setGg_value(value);
		translations.setId(id);
		translations.setModel(module);
		//map.addAllAttributes("translations",translations);
		System.out.println(translations);
		transFacade.addTransConfiguration(translations);
		String message= "Configuration " +id + " registered successfully";
		ModelAndView mav=new ModelAndView();
		mav.setViewName("success");
		mav.addObject("message", message);
		System.out.println("mav :"+mav.getViewName());
		System.out.println("mav :"+mav.getModel());
		//res.sendRedirect("http://localhost:8080/GGTranslationsWeb/src/main/webapp/WEB-INF/js/success.jsp");
		//model.addAttribute("success", "Configuration " +id + " registered successfully");
		
		//return  new ModelAndView("success", "message", message);
		return "redirect:success";
	}
	
	@RequestMapping(method = RequestMethod.GET,value = "/listConfig" ,produces= org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String listConfiguration() throws Exception {
		System.out.println("its here in list controller");
		
		String JSONList = transFacade.getAllTransConfiguration();
		System.out.println(JSONList);
		/*return  new ModelAndView("listConfiguration", "translationsList", JSONList);
		Map<String,Object> result = new HashMap<>();
		result.put("translationsList", JSONList);*/
		return JSONList;
	}
	
	

}
