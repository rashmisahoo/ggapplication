<!-- transConfiguration.jsp -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Translations Configuration</title>
</head>
<body>
 	<form action="addConfig" method="POST">
		<table>
		
			<tr>
				<td>Module ID :</td>
				<td><input type="text" name="id"></td>
			</tr>
			
			<tr>
				<td>Module Name :</td>
				
				<td><input type="text" name="module"></td>
			</tr>
			<tr>
				<td>Key Name :</td>
				
				<td><input type="text" name="key"></td>
			</tr>
			<tr>
				<td>Value Details :</td>
				
				<td><input type="text" name="value"></td>
			</tr>
			<tr>
				<td><input type="submit" value="add"></td>
				
			</tr>
		</table>
	</form>
	<br/>
    <br/>
   			<a href="<c:url value='/listConfig' />">List of All Configurations</a>
	<br>
 
</body>
</html>