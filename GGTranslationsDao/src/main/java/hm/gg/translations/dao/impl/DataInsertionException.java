package hm.gg.translations.dao.impl;

public class DataInsertionException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String toString(){
		return "DataInsertionException: Data is Not inserted";
	}

}
